package com.epam.esm.controller;



import java.io.IOException;
import java.math.BigDecimal;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import com.epam.esm.dao.GiftCertificateDao;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import com.epam.esm.dao.TagDao;

@Controller
public class ApiController {

	@Autowired
	private final TagDao tagDao;

	@Autowired
	private final GiftCertificateDao giftDao;



	public ApiController (TagDao tagDao, GiftCertificateDao giftDao) {
		this.tagDao = tagDao;
		this.giftDao = giftDao;

	}

	@RequestMapping(value="/")
	public ModelAndView test(HttpServletResponse response) throws IOException{
		return new ModelAndView("home");
	}

	@RequestMapping(value = "/gift",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody GiftCertificate getGift(HttpServletResponse response) throws IOException{
		GiftCertificate gift = new GiftCertificate();
		gift.setId(1);
		gift.setName("My gift");
		gift.setPrice(new BigDecimal(12.34));
		gift.setDuration(0);
		Date date = new Date();
		gift.setCreateDate(date);
		gift.setLastUpdateDate(date);
		return gift;
	}

	@RequestMapping(value="/gift",
			method=RequestMethod.POST)
	public @ResponseBody GiftCertificate postGift(@RequestBody GiftCertificate gift) throws IOException{
		giftDao.insert(gift);
		return gift;
	}

	@RequestMapping(value="/tag",
			method=RequestMethod.POST)
	public @ResponseBody Tag postGift(@RequestBody Tag tag) throws IOException {
		tagDao.insert(tag);
		return tag;
	}


}