package com.epam.esm.dao;


import com.epam.esm.model.GiftCertificate;

import java.util.List;
public interface GiftCertificateDao {

    GiftCertificate findById(int id);
    List<GiftCertificate> findAll();
    void insert (GiftCertificate giftCertificate);
    void update(GiftCertificate giftCertificate);
    void delete(int id);
}
